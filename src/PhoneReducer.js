import { UPDATE_PHONE_NUMBER, POP_PHONE_NUMBER } from './actions/PhoneNumberActions'

const initialState = {
  inputValue: '',
  valueHistory: []
}

export default function phoneNumber(state = initialState, action) {
  console.log('state', state.valueHistory, action)
  switch (action.type) {
    case UPDATE_PHONE_NUMBER:
      state.valueHistory.push(state.inputValue)
      return Object.assign({}, state, {
        inputValue: action.value,
        valueHistory: state.valueHistory
      })
    case POP_PHONE_NUMBER:
      if(state.valueHistory.length) {
        return Object.assign({}, state, {
          inputValue: state.valueHistory.pop(),
          valueHistory: state.valueHistory
        })
      } else {
        return state
      }
    default:
      return state
  }
}