import React from 'react'
import { Paper, Button, Switch } from '@material-ui/core'

class OppositePhoneStatus extends React.Component {

  constructor(props) {
    super(props)
    this.state = { visible: true, toggleValue: true }
  }

  handleChange = event => {
    this.setState({ ...this.state, toggleValue: event.target.checked });
  };

  render() {
    const isValidNumber = this.props.isValidPhoneNumber
    const { toggled } = this.props
    const { visible } = this.state
    return (
      <Paper style={{padding: '15px'}}>
        {visible && (<Paper style={{padding: '15px', backgroundColor: isValidNumber || toggled ? 'green' : 'red'}}>
          <Switch
            checked={toggled}
            onChange={ () => this.props.onToggle()}
            value="checked"
            color="primary"
          />
          </Paper>)}
          <Button variant="contained" onClick={() => this.setState({visible:!visible})}>{ visible ? 'Close' : 'Open' }</Button>
      </Paper>
    )
  }
}

export default OppositePhoneStatus
