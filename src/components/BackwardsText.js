import React from 'react'
import { Paper, Button, Switch, TextField } from '@material-ui/core'
import { connect } from 'react-redux'
import { updatePhoneNumber } from '../actions/PhoneNumberActions'

class BackwardsText extends React.Component {

  constructor(props) {
    super(props)
    this.state = { visible: true, toggleValue: false, inputValue: props.phoneNumber }
    setInterval(() => this.popOff(), 1000)
  }

  popOff() {
    if (this.props.toggled) {
      // wasn't sure if I should be mutating store value or local state for this
      this.props.updatePhone(this.props.phoneNumber.slice(0, this.props.phoneNumber.length - 1 ));
      // this.setState({inputValue: this.state.inputValue.slice(0, this.state.inputValue.length - 1)})
    }
  }

  // handleChange = event => {
  //   this.setState({ ...this.state, toggleValue: event.target.checked });
  // };

  render() {
    const inputValue = this.props.phoneNumber
    // const { inputValue } = this.state
    const { toggled } = this.props
    const flippedValue = inputValue.length % 2 ? inputValue.split('').reverse().join('') : inputValue
    const { visible } = this.state
    return (
      <div style={{padding: '15px'}}>
        {visible && (<Paper>
          <TextField disabled value={flippedValue} />
          <Switch
            checked={toggled}
            onChange={ () => this.props.onToggle()}
            value="checked"
            color="primary"
          />
        </Paper>)}
        <Button variant="contained" onClick={() => this.setState({visible:!visible})}>{ visible ? 'Close' : 'Open' }</Button>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updatePhone: value => {
      dispatch(updatePhoneNumber(value))
    },
  }
}

export default connect(null, mapDispatchToProps)(BackwardsText)
