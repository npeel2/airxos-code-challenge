import React from 'react'
import { Grid, TextField, Paper, Button } from '@material-ui/core'
import { connect } from 'react-redux'
import { updatePhoneNumber, popPhoneNumber } from '../actions/PhoneNumberActions'
import PhoneNumberStatus from './PhoneNumber'
import OppositePhoneStatus from './OppositePhoneStatus'
import BackwardsTextStatus from './BackwardsText'

class InputContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = { orToggled: false, backwardsToggled: false }
  }

  updateInputValue(event) {
    console.log('value', event.target.value)
    // this.setState({phoneNumber: event.target.value})
    this.props.updatePhone(event.target.value)
  }

  isPhoneNumber(value) {
    var phoneNumberPattern = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/
    return phoneNumberPattern.test(value)
  }


  render() {
    const { phoneNumber, valueHistory } = this.props
    const { orToggled, backwardsToggled } = this.state
    // no need to check second component state is 'true', allValid would be false if phone number is invalid
    const allValid = this.isPhoneNumber(phoneNumber) && this.state.backwardsToggled
    console.log('ph', phoneNumber, valueHistory, this.props.popPhoneNumber)
    const isValid = this.isPhoneNumber(phoneNumber)
    return (<Grid container direction="column">
      <Grid item style={{padding: '15px'}}>
        <Paper style={{padding: '15px'}}>
          <TextField style={{margin: '5px'}} label="Phone Number" onChange={(event) => this.updateInputValue(event) } value={phoneNumber} name="phone" />
          <Button style={{margin: '5px'}} color="secondary" onClick={()=> {this.props.popPhoneNumber()}}>Undo</Button>
          <Button style={{margin: '5px'}} variant="contained" color="primary" disabled={!allValid} >Continue</Button>
        </Paper>
      </Grid>
      <Grid item style={{padding: '15px'}}>
        <Paper>
          <PhoneNumberStatus phoneNumber={phoneNumber} isValidPhoneNumber={isValid} />
        </Paper>
      </Grid>
      <Grid item style={{padding: '15px'}}>
        <Paper>
          <OppositePhoneStatus phoneNumber={phoneNumber} isValidPhoneNumber={isValid} toggled={orToggled} onToggle={() => {this.setState({orToggled: !orToggled})}} />
        </Paper>
      </Grid>
      <Grid item style={{padding: '15px'}}>
        <Paper>
          <BackwardsTextStatus phoneNumber={phoneNumber} isValidPhoneNumber={isValid} toggled={backwardsToggled} onToggle={() => {this.setState({backwardsToggled: !backwardsToggled})}}/>
        </Paper>
      </Grid>
    </Grid>)
  }
}

const mapStateToProps = (state) => {
  return {
    phoneNumber: state.inputValue,
    valueHistory: state.valueHistory,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updatePhone: value => {
      dispatch(updatePhoneNumber(value))
    },
    popPhoneNumber: () => dispatch(popPhoneNumber())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(InputContainer)
