import React from 'react'
import { Paper, Button, Typography } from '@material-ui/core'

class PhoneNumber extends React.Component {

  constructor(props) {
    super(props)
    this.state = { visible: true }
  }


  render() {
    const { visible } = this.state
    const validNumber = this.props.isValidPhoneNumber
    return (
      <Paper style={{padding: '15px'}}>
        {visible && (<Paper style={{padding: '15px', backgroundColor: validNumber ? 'green' : 'red'}}>
          <Typography>{validNumber ? 'Phone number is valid' : 'Phone number is invalid'}</Typography>
        </Paper>)}
        <Button variant="contained" onClick={() => this.setState({ visible: !visible })}>{ visible ? 'Close' : 'Open' }</Button>
      </Paper>
    )
  }
}

export default PhoneNumber
