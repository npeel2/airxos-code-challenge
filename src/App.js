import React from 'react';
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import phoneReducer from './PhoneReducer'
// import { ThemeProvider } from '@material-ui/core/styles';
import InputContainer from './components/InputContainer';

const store = createStore(phoneReducer)

function App() {
  return (
    <Provider store={store}>
      <InputContainer />
    </Provider>
  );
}

export default App;
