
export const UPDATE_PHONE_NUMBER = 'UPDATE_PHONE_NUMBER'
export function updatePhoneNumber(value) {
  return {
    type: UPDATE_PHONE_NUMBER,
    value
  }
}

export const POP_PHONE_NUMBER = 'POP_PHONE_NUMBER'
export function popPhoneNumber() {
  return {
    type: POP_PHONE_NUMBER
  }
}